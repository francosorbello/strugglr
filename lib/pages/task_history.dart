import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';

class TaskHistory extends StatelessWidget {
  final String history;
  const TaskHistory({required this.history, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var historyList = history.split(",");
    historyList = historyList.reversed.toList();
    return Scaffold(
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: Colors.deepPurple[700],
        title: Text("Task history"),
        elevation: 0,
      ),
      body: history == "" ?
      Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Center(
          child: Text(
            "You dont have a record history in this task yet.",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey[700],
              fontSize: 20,
            ),
          ),
        ),
      )
      :
      Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView.builder(
            itemCount: historyList.length,
            scrollDirection: Axis.vertical,
            itemBuilder: (context,index) {
              String record = historyList[index];
              return Padding(
                padding: const EdgeInsets.fromLTRB(0,8,0,8),
                child: Text(
                  "$record days",
                  style: TextStyle(fontSize: 20),
                ),
              );
            }
        ),
      ),
    );
  }
}
