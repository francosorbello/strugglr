import 'package:flutter/material.dart';
import 'package:strugglr/db/SettingsDatabase.dart';
import 'package:strugglr/db/TaskDatabase.dart';
import 'package:strugglr/model/task.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:strugglr/model/task_updater.dart';
//import 'package:strugglr/model/bgTask.dart';
import 'package:strugglr/model/task_updater.dart';
import 'package:flutter/foundation.dart';


class Loading extends StatefulWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {

  @override
  void initState(){
    super.initState();
    SetupApp();
  }

  void SetupApp() async {
    //Task nTask = Task(title: "hola",currentRecord: 27,history: "1");
    //await TaskDatabase.instance.createTask(nTask);
    await TaskDatabase.instance.readAllTasks();
    WidgetsFlutterBinding.ensureInitialized();
    await updateRecords();
    await Future.delayed(Duration(seconds: 1));
    Navigator.pushReplacementNamed(context, "/home");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.deepPurple[700],
        body: Center(
          child: SpinKitRing(
            color: Colors.white,
            size: 50.0,
          ),
        )
    );
  }
}