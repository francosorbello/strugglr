import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:strugglr/db/TaskDatabase.dart';
import 'package:strugglr/model/task.dart';

class AddTaskView extends StatefulWidget {
  const AddTaskView({Key? key}) : super(key: key);

  @override
  _AddTaskViewState createState() => _AddTaskViewState();
}

class _AddTaskViewState extends State<AddTaskView> {
  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  bool emptyTitle = false;


  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    titleController.dispose();
    super.dispose();
  }

  void AddTask(String title) async {
    if(title == "") return;
    final nTask = Task(
      title: title,
      currentRecord: 0,
      history: "",
      createdTime: DateTime.now(),
    );
    await TaskDatabase.instance.createTask(nTask);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        title: Text("Add new task"),
        backgroundColor: Colors.deepPurple[700],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          if(titleController.text == ""){
            setState(() {
              emptyTitle = true;
            });
            return;
          }
          AddTask(titleController.text);
        },
        child: Icon(Icons.check),
        backgroundColor: Colors.deepPurple[700],
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              TextFormField(
                maxLines: 1,
                controller: titleController,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                ),
                decoration: InputDecoration(
                  hintText: "Title",
                  hintStyle: TextStyle(color: Colors.grey[300]),
                  errorText: emptyTitle ? "Title cant be empty" : "",
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.deepPurple[700]!
                    )
                  )
                ),
                cursorColor: Colors.black,
              )
            ],
          ),
        ),
      ),
    );
  }
}
