import 'package:flutter/material.dart';

class ImageList extends StatelessWidget {
  const ImageList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var imageList = [];
    return Container(
      child: GridView.count(
        crossAxisCount: 1,
        mainAxisSpacing: 20,
        children: [
          Image(image: AssetImage("assets/b1.png"),),
          Image(image: AssetImage("assets/b2.png"),),
          Image(image: AssetImage("assets/b3.png"),),
          Image(image: AssetImage("assets/b4.png"),),
          Image(image: AssetImage("assets/b5.png"),),
          Image(image: AssetImage("assets/b6.png"),),
          Image(image: AssetImage("assets/b7.png"),),
          Image(image: AssetImage("assets/b8.png"),),
        ],
      ),
    );
  }
}
