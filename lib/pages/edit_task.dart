import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:strugglr/db/TaskDatabase.dart';
import 'package:strugglr/model/task.dart';

class EditTaskView extends StatefulWidget {
  final Task task;
  const EditTaskView({required this.task, Key? key}) : super(key: key);

  @override
  _EditTaskViewState createState() => _EditTaskViewState();
}

class _EditTaskViewState extends State<EditTaskView> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController titleController;
  bool emptyTitle = false;

  @override
  void initState()
  {
    super.initState();
    titleController = new TextEditingController(text: widget.task.title);
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    titleController.dispose();
    super.dispose();
  }

  void UpdateTask(String title) async {
    if(title == "") return;
    final nTask = widget.task.copy(title: title);
    await TaskDatabase.instance.updateTask(nTask);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        title: Text("Edit task"),
        backgroundColor: Colors.deepPurple[700],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          if(titleController.text == ""){
            setState(() {
              emptyTitle = true;
            });
            return;
          }
          UpdateTask(titleController.text);
        },
        child: Icon(Icons.check),
        backgroundColor: Colors.deepPurple[700],
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              TextFormField(
                maxLines: 1,
                controller: titleController,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                ),
                decoration: InputDecoration(
                    hintText: "Title",
                    hintStyle: TextStyle(color: Colors.grey[300]),
                    errorText: emptyTitle ? "Title cant be empty" : "",
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.deepPurple[700]!
                        )
                    )
                ),
                cursorColor: Colors.black,
              )
            ],
          ),
        ),
      ),
    );
  }
}
