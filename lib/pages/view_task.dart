import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:strugglr/db/TaskDatabase.dart';
import 'package:strugglr/model/task.dart';
import 'package:strugglr/pages/circle_page.dart';
import 'package:strugglr/pages/edit_task.dart';
import 'package:strugglr/pages/task_history.dart';
import 'package:strugglr/pages/popup_menu.dart';

class ReadTaskView extends StatefulWidget {
  final int taskId;
  const ReadTaskView({required this.taskId, Key? key}) : super(key: key);

  @override
  _ReadTaskViewState createState() => _ReadTaskViewState();
}

class _ReadTaskViewState extends State<ReadTaskView> {
  late Task task;
  bool loading = false;

  void stopTask(BuildContext context) async{
    int lastRecord = task.currentRecord;
    String nHistory = task.history;
    nHistory += nHistory == "" ? lastRecord.toString() : ","+lastRecord.toString();

    final nTask = task.copy(currentRecord: 0,history: nHistory,createdTime: DateTime.now());
    await TaskDatabase.instance.updateTask(nTask);

    setState(() {
      this.task = nTask;
    });
  }

  void setTaskById() async {
    setState(() {
      loading = true;
    });
    Task? nTask = await TaskDatabase.instance.readTask(widget.taskId);
    print("pulled task ${nTask!.title} (id: ${nTask.id}) with ${nTask.currentRecord} days");
    if(nTask != null){
      setState(() {
        this.task = nTask;
        loading = false;
      });
    } else {
      print("fuck");
    }
  }

  @override
  void initState() {
    super.initState();
    setTaskById();
  }

  Future<void> stopTaskDialog(Task task,BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Reset task"),
            content: Text("Are you sure you want to restart this task?"),
            actions: [
              TextButton(
                onPressed: () {
                  stopTask(context);
                  Navigator.of(context).pop();
                },
                child: Text("Yes",
                  style: TextStyle(
                    color: Colors.red,
                  ),),
              ),
              TextButton(
                onPressed: (){
                  Navigator.of(context).pop();
                },
                child: Text("No"),
              )
            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    if(this.loading){
      return Center(
          child: SpinKitRing(
            color: Colors.deepPurple[700]!,
            size: 50.0,
          )
      );
    }
    return Scaffold(
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: Colors.deepPurple[700],
        title: Text("View task"),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  color: Colors.deepPurple[700],
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "${task.title}",
                      style: TextStyle(
                        fontSize: 40,
                        color: Colors.white,
                        //backgroundColor: Colors.deepPurple[900],
                      ),
                    ),
                  ),
                ),
              ),
              //Divider(),
              //SizedBox(height: 50),
              Expanded(
                flex: 5,
                child: Stack(
                    fit: StackFit.expand,
                    children: [
                      Container(
                        child: CustomPaint(
                          painter: CirclePainter(),
                          child: Center(
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                  "${task.currentRecord} days",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 50,
                                  ),
                          ),
                            )),
                        ),
                      ),
                    ])
              ),
              Expanded(
                flex: 1,
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: IconButton(
                        tooltip: "Edit task",
                        onPressed: () async{
                          await Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) =>EditTaskView(task: task,)
                              )
                          );
                          setState(() {
                            setTaskById();
                          });
                        },
                        icon: Icon(Icons.edit,color: Colors.deepPurple[700],size: 40,),
                      ),
                    ),
                  Expanded(
                    flex: 1,
                    child: IconButton(
                        tooltip: "History",
                        onPressed: (){
                          Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) =>TaskHistory(history: task.history,)
                              )
                          );
                        },
                        icon: Icon(Icons.history,color: Colors.deepPurple[700],size: 40,),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: IconButton(
                        tooltip: "Stop task",
                        onPressed: (){
                          stopTaskDialog(task, context);
                        },
                        icon: Icon(Icons.stop,color: Colors.deepPurple[700],size: 40,),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
    );
  }
}
