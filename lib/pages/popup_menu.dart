import 'package:flutter/material.dart';
import 'package:strugglr/pages/image_list.dart';
//import 'package:strugglr/pages/loading.dart';

class TaskListMenu extends StatelessWidget {
  const TaskListMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PopupMenuButton(
        icon: Icon(Icons.more_vert),
        itemBuilder: (context) => <PopupMenuEntry>[
          PopupMenuItem(
              child: ListTile(
                leading: Icon(Icons.art_track),
                title: Text("Motivational art"),
                onTap: () async{
                  await Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ImageList(),
                      )
                  );
                  Navigator.of(context).pop();
                },
              )
          ),
        ],
      ),
    );
  }
}
