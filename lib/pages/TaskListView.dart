import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:strugglr/db/TaskDatabase.dart';
import 'package:strugglr/model/task.dart';
import 'package:strugglr/pages/add_task.dart';
import 'package:strugglr/pages/popup_menu.dart';
import 'package:strugglr/pages/view_task.dart';

class TaskListView extends StatefulWidget {
  const TaskListView({Key? key}) : super(key: key);

  @override
  _TaskListViewState createState() => _TaskListViewState();
}

class _TaskListViewState extends State<TaskListView> {
  late List<Task> tasks;
  bool loading = false;

  @override
  void initState() {
    super.initState();
    refreshTasks();
  }

  void refreshTasks() async{
    setState(() {
      loading = true;
    });
    this.tasks = await TaskDatabase.instance.readAllTasks();
    setState(() {
      loading = false;
    });
  }

  Future<void> deleteTaskDialog(Task task) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Delete task"),
            content: Text("Are you sure you want to delete this task?"),
            actions: [
              TextButton(
                onPressed: () async{
                  await TaskDatabase.instance.deleteTask(task);
                  Navigator.of(context).pop();
                },
                child: Text("Yes",
                    style: TextStyle(
                      color: Colors.red,
                    ),),
              ),
              TextButton(
                onPressed: (){
                  Navigator.of(context).pop();
                },
                child: Text("No"),
              )
            ],
          );
        }
    );
  }

  Future<Task?> updateTask(Task task) async{
    final nTask = await TaskDatabase.instance.readTask(task.id!);
    return nTask;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backwardsCompatibility: false,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: Colors.deepPurple[700],
        title: Text("Strugglr"),
        elevation: 0,
        actions: [
          TaskListMenu(),
        ],
      ),
      floatingActionButton: OpenContainer(
        transitionDuration: Duration(milliseconds: 500),
        closedBuilder: (context,openContainer) => Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.deepPurple[700],
          ),
          height: 60,
          width: 60,
          child: Icon(Icons.add,color: Colors.white,),
        ),
        openBuilder: (context, _)=> AddTaskView(),
        closedShape: CircleBorder(),
        closedColor: Colors.deepPurple[700]!,
        transitionType: ContainerTransitionType.fadeThrough,
        onClosed: (_) => refreshTasks(),
      ),
      body: loading ?
      Center(
          child: SpinKitRing(
            color: Colors.deepPurple[700]!,
            size: 50.0,
          ))
          :
      ListView.builder(
          itemCount: tasks.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (context,index) {
            Task task = tasks[index];

            return TextButton(
                onPressed: () async {
                  print("opening task ${task.title} (id: ${task.id}) on index $index");
                  await Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ReadTaskView(taskId: task.id!,),
                      )
                  );
                  setState(() {
                    refreshTasks();
                  });
                },
                onLongPress: () async{
                    //await TaskDatabase.instance.deleteTask(task);
                    await deleteTaskDialog(task);
                    refreshTasks();
                },
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "${task.title}",
                    style: TextStyle(
                      color: Colors.deepPurple[700],
                    ),
                  ),
                ),
            );
          }
      )
    );
  }
}
