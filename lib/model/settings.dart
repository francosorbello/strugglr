final String settings_tablename = "settings";

class SettingsFields{
  static final List<String> values = [
    id, key, value,
  ];

  static final String id = "_id";
  static final String key = "key";
  static final String value = "value";

}