import 'package:flutter/material.dart';
import 'package:strugglr/db/SettingsDatabase.dart';
import 'package:strugglr/db/TaskDatabase.dart';
import 'package:strugglr/model/task.dart';

Future updateRecords() async{
  List<Task> tasks = await TaskDatabase.instance.readAllTasks();
  DateTime lastTime;

  var lastUpdated = await SettingsDatabase.instance.getSetting("lastUpdated");
  if(lastUpdated == null) {
    await SettingsDatabase.instance.setSetting("lastUpdated", DateTime.now().toIso8601String());
    lastTime = DateTime.now();
  } else {
    lastTime = DateTime.parse(lastUpdated);
  }

  print("last time: $lastTime");

  if(daysBetween(lastTime, DateTime.now()) < 1) return;

  print(tasks.length);
  for(int i=0; i < tasks.length; i++) {
    int days = daysBetween(tasks[i].createdTime, DateTime.now());
    final nTask = tasks[i].copy(currentRecord: days);
    print("updated ${nTask.title}(id: ${nTask.id}) to ${nTask.currentRecord} days");
    await TaskDatabase.instance.updateTask(nTask);
  }

  await SettingsDatabase.instance.setSetting("lastUpdated", DateTime.now().toIso8601String());
}

int daysBetween(DateTime from, DateTime to) {
  from = DateTime(from.year, from.month, from.day);
  to = DateTime(to.year, to.month, to.day);
  return (to.difference(from).inHours / 24).round();
}