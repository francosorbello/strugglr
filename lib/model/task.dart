final String tableName = "tasks1";

class TaskFields{
  static final List<String> values = [
    id, title, currentRecord, history, time
  ];

  static final String id = "_id";
  static final String title = "title";
  static final String currentRecord = "currentRecord";
  static final String history = "history";
  static final String time = "time";
}

class Task{
  final int? id;
  final String title;
  final int currentRecord;
  final String history;
  final DateTime createdTime;

  const Task({
    this.id,
    required this.title,
    required this.currentRecord,
    required this.history,
    required this.createdTime,
  });

  Map<String, Object?> toJson() => {
    TaskFields.id: id,
    TaskFields.title: title,
    TaskFields.currentRecord: currentRecord,
    TaskFields.history: history,
    TaskFields.time: createdTime.toIso8601String(),
  };

  Task copy({
      int? id,
      String? title,
      int? currentRecord,
      String? history,
      DateTime? createdTime,
    }) {
      return Task(
        id: id ?? this.id,
        title: title ?? this.title,
        currentRecord: currentRecord ?? this.currentRecord,
        history: history ?? this.history,
        createdTime: createdTime ?? this.createdTime,
    );
  }

  static Task fromJson(Map<String,Object?> json) {
    var timeString = json[TaskFields.time];
    timeString = timeString == null ? DateTime.now().toIso8601String() : json[TaskFields.time] as String;
    final createdTime = DateTime.parse(timeString as String);

    return Task(
      id: json[TaskFields.id] as int?,
      title: json[TaskFields.title] as String,
      currentRecord: json[TaskFields.currentRecord] as int,
      history: json[TaskFields.history] as String,
      createdTime: createdTime,
    );
  }
}