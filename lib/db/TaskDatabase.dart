import 'dart:async';
import 'package:strugglr/model/task.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class TaskDatabase {
  static final TaskDatabase instance = TaskDatabase._init();
  static Database? _database;
  TaskDatabase._init(); //named constructor vacio que inicializa el singleton

  Future<Database> get database async {
    if(_database != null) {
      return _database!;
    };
    _database = await _initDB("tasks1.db");

    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath,filePath);
    return await openDatabase(path,version: 1,onCreate: _createDb);
  }

  Future _createDb(Database db, int version) async{
    await db.execute('DROP TABLE If EXISTS $tableName');

    final idType = "INTEGER PRIMARY KEY AUTOINCREMENT";
    final textType = "TEXT NOT NULL";
    final intType = "INTEGER NOT NULL";

    await db.execute(
      '''
        CREATE TABLE $tableName (
          ${TaskFields.id} $idType,
          ${TaskFields.title} $textType,
          ${TaskFields.currentRecord} $intType,
          ${TaskFields.history} $textType,
          ${TaskFields.time} $textType
        )
      '''
    );
  }

  Future<Task> createTask(Task task) async {
    final db = await instance.database;
    final id = await db.insert(tableName, task.toJson());

    return task.copy(id: id);
  }

  Future<Task?> readTask(int id) async {
    final db = await instance.database;

    final maps = await db.query(
      tableName,
      columns: TaskFields.values,
      where: "${TaskFields.id} = ?",
      whereArgs: [id],
    );
    if (maps.isNotEmpty) {
      return Task.fromJson(maps.first);
    }
    else {
      return null;
    }
  }

  Future<List<Task>> readAllTasks() async{
    final db = await instance.database;
    //debugger();
    final result = await db.query(tableName);
    return result.map((json)=>Task.fromJson(json)).toList();
  }

  Future<int> updateTask(Task task) async {
    final db = await instance.database;
    return db.update(
      tableName,
      task.toJson(),
      where: "${TaskFields.id} = ?",
      whereArgs: [task.id]
    );
  }

  Future<int> deleteTask(Task task) async {
    final db = await instance.database;

    return db.delete(
      tableName,
      where: "${TaskFields.id} = ?",
      whereArgs: [task.id]
    );
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}