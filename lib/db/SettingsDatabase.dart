import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:strugglr/model/settings.dart';

class SettingsDatabase {
  static final SettingsDatabase instance = SettingsDatabase._init();
  static Database? _database;
  SettingsDatabase._init(); //named constructor vacio que inicializa el singleton

  Future<Database> get database async {
    if(_database != null) {
      return _database!;
    };
    _database = await _initDB("settings.db");

    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath,filePath);
    bool fuck = await databaseExists(path);
    return await openDatabase(path,version: 1,onCreate: _createDb);
  }

  Future _createDb(Database db, int version) async{
    await db.execute('DROP TABLE If EXISTS $settings_tablename');

    final idType = "INTEGER PRIMARY KEY AUTOINCREMENT";
    final textType = "TEXT NOT NULL";
    final intType = "INTEGER NOT NULL";

    await db.execute(
        '''
        CREATE TABLE $settings_tablename (
          ${SettingsFields.id} $idType,
          ${SettingsFields.key} $textType,
          ${SettingsFields.value} $textType
        )
      '''
    );
  }

  Future<void> removeSetting (String key) async{
    final db = await instance.database;

    await db.delete(
      settings_tablename,
      where: "${SettingsFields.key} = ?",
      whereArgs: [key],
    );
  }

  Future<String?> getSetting(String key) async{
    final db = await instance.database;

    final setting = await db.query(
      settings_tablename,
      columns: SettingsFields.values,
      where: "${SettingsFields.key} = ?",
      whereArgs: [key],
    );
    if (setting.isNotEmpty) {
      print(setting.first);
      return setting.first[SettingsFields.value] as String;
    }
    else {
      return null;
    }
  }

  Future<int> setSetting (String key, String value) async{
    final db = await instance.database;
    bool containsKey = await this.containsKey(key);

    final newSetting = {
      SettingsFields.key: key,
      SettingsFields.value: value,
    };

    if(containsKey){
      //print("updating setting $key with value $value");
      return await db.update(
        settings_tablename,
        newSetting,
        where: "${SettingsFields.key} = ?",
        whereArgs: [key],
      );
    }

    //print("creating new setting with pair $key,$value");
    return await db.insert(settings_tablename, newSetting);
  }

  Future<bool> containsKey(String key) async{
    final db = await instance.database;
    final keys = await db.query(
        settings_tablename,
        columns: [SettingsFields.key],
    );

    for(int i=0; i<keys.length; i++){
      if(keys[i][SettingsFields.key] == key) return true;
    }
    return false;
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}